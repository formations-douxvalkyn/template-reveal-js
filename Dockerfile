FROM alpine:latest

# Récupération de la dernière version de reveal.js
RUN wget -O reveal.zip https://github.com/hakimel/reveal.js/archive/master.zip

# Extraction de l'archive
RUN unzip reveal.zip -d reveal

# Création d'un dossier public pour stocker notre future présentation
RUN mkdir /public

# Copie des assets nécessaires reveal.js
RUN mv reveal/reveal.js-master/dist /public/
RUN mv reveal/reveal.js-master/plugin /public/

# Copie du fichier index.html modifié (gestion du markdown, et lien vers le fichier css custom)
COPY ./index.html /public/

# Copie des styles custom
RUN mkdir /public/custom
COPY ./custom.css /public/custom/
